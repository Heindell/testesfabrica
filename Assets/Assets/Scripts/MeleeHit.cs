﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeHit : MonoBehaviour
{
    public int damage;
    public GameObject impactEffect;
    
    void OnTriggerEnter2D(Collider2D other) {
        Enemy enemy = other.GetComponent<Enemy>();
        if (other.isTrigger != true && other.CompareTag("Enemy")){
            enemy.TakeDamage(damage);
            Instantiate(impactEffect, transform.position, transform.rotation);
        }
        Instantiate(impactEffect, transform.position, transform.rotation);
    }
}
