﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDash : MonoBehaviour
{
    public float range;
    public Transform player;
    private Rigidbody2D rb;
    public float dashSpeed;
    private float dashTime;
    public float startDashTime;
    private float dashInterval = 5f;
    private float nextDash;
    // Start is called before the first frame update
    void Start()
    {
         rb = GetComponent<Rigidbody2D>();
        dashTime = startDashTime;
    }

    // Update is called once per frame
    void Update()
    {
        if(dashTime <= 0){
                dashTime = startDashTime;
                rb.velocity = Vector2.zero;
            }

        if (Vector3.Distance(player.position, transform.position) <= range){
            if(Time.time > nextDash){
            rb.velocity = Vector2.left * dashSpeed;
            
            nextDash = Time.time + dashInterval;
            }
        }
    }
}
