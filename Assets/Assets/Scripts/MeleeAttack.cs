﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttack : MonoBehaviour
{
    bool canMelee = true;
    public float meleeCd = 0.3f;
    public Collider2D MeleeAttackBox;
    // Start is called before the first frame update

    void Awake() 
    {
        MeleeAttackBox.enabled = false;       
    }
    // Update is called once per frame
    void Update()
    {
        if(canMelee && Input.GetButtonDown("Melee")){
            StartCoroutine("Melee");
            
        }
        
    }

    IEnumerator Melee(){
        MeleeAttackBox.enabled = true;
        canMelee = false;
        yield return new WaitForSeconds(meleeCd);
        canMelee = true; 
        MeleeAttackBox.enabled = false;

    }
}
