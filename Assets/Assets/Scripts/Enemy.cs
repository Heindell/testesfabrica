using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	public int healthp = 100;

	public GameObject deathEffect;
	bool poisonEffect = true;
bool isPoisoned = false;
public int poisonDamage = 5;
public float poisonTimer = 1f;
public Hearts playerHearts;
public float damageTimer = 1f;
private bool canDealDamage = true;
public int hitDamage = 1;


void Update() {
	if(isPoisoned){
            StartCoroutine("PoisonDamage");
        }
	if (healthp <= 0)
		{
			Die();
		}	
}

	public void TakeDamage (int damage)
	{
		healthp -= damage;
		

		if (healthp <= 0)
		{
			Die();
		}
	}

	void Die ()
	{
		Instantiate(deathEffect, transform.position, Quaternion.identity);
		Destroy(gameObject);
	}

	IEnumerator PoisonDamage(){

                 
        
        if(poisonEffect){
            healthp -= poisonDamage;
            poisonEffect = false;
            yield return new WaitForSeconds(poisonTimer);
            poisonEffect = true;
        }

    }

	private void OnTriggerStay2D(Collider2D other) {
		if (canDealDamage && other.gameObject.tag == "Player"){
			playerHearts.curHearts -= hitDamage;
			StartCoroutine("DamageDelay");
		}
	}

	IEnumerator DamageDelay(){
		canDealDamage = false;
		yield return new WaitForSeconds(damageTimer);
		canDealDamage = true;
	}

}
