using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabWeapon : MonoBehaviour {

	public Transform firePoint;
	public Transform firePointCrouched;
	public GameObject bulletPrefab;
	public PlayerMovement player;
	bool canShoot = true;
	public float shotDelay = 0.5f;
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Fire1"))
		{
			if (canShoot){
			StartCoroutine("ShootT");
			}
		}
	}

	void Shoot ()
	{
		if(!player.crouch){
		Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
		}
		else{
			Instantiate(bulletPrefab, firePointCrouched.position, firePoint.rotation);
		}
	}

	IEnumerator ShootT(){
		Shoot();
		canShoot = false;
		yield return new WaitForSeconds(shotDelay);
		canShoot = true;
	}
}
