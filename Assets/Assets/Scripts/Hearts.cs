﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hearts : MonoBehaviour
{
    public int curHearts;
    public int maxHearts;
    public Image[] hearts;
    public Sprite fullHeart;
    public Sprite emptyHeart;
    // Update is called once per frame
    void Update()
    {
        if (curHearts > maxHearts){
            curHearts = maxHearts;
        }

        for (int i = 0; i < hearts.Length; i++){
            if (i<curHearts){
                hearts[i].sprite = fullHeart;
            }
            else{
                hearts[i].sprite = emptyHeart;
            }

            if (i < maxHearts){
                hearts[i].enabled = true;
            }
            else
            {
                hearts[i].enabled = false;
            }
        }
    }
}
